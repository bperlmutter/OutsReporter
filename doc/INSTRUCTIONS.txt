/*
    Outs Reporter - A program written for Time Service Inc. intended to
	produce "Outs Reports" given a department inventory and an authorized
	inventory list.

	Copyright (C) 2016 Benjamin Perlmutter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



In order to use this program, follow these instructions.

Outs Reporter is a program written for Time Service Inc. intended to
produce "Outs Reports" given a department inventory and an authorized
inventory list.

1. Open the program (double-click OutsReporter.jar)

2. Click the text boxes and select files from the menus that appear.
	The inventory file must be .xls format and must be named as [number].xls
	The number should correspond to the department number of the store

	The file must contain columns headed 'store' 'sku' 'quantity' and 'status'
	The columns may start anywhere in the file, and their headings are NOT
	case sensitive. This program has been thoroughly tested with the columns
	starting in the top row of document, and it is possible that troubles
	will arise if the file is formatted differently. (The format tested is the
	format that inventory files are currently set in, as of January 2016).

	The second file (authFile) must be titled SKUSTROUT.csv and must contain
	columns as follows:
	First column: department numbers
	Fourth column: Time Service's SKU
	Fifth column: Sears' SKU

	All other columns are optional. Column headings may cause trouble, but
	should not necessarily be a problem.

3. Click "Generate Report" to generate the outs report.

	You should be notified of any errors as you run the program. 
	You will be notified when an Outs Report is successfully generated.
	If you are not notified, the Outs Report was not successfully generated.




