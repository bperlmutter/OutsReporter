/*
    Outs Reporter - A program written for Time Service Inc. intended to
	produce "Outs Reports" given a department inventory and an authorized
	inventory list.

	Copyright (C) 2016 Benjamin Perlmutter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



Note: All files are configured to work on a machine running OS X. It is likely 
that this will work on and UNIX machine, and it is likely that if path names 
are edited to reflect Windows path names, then the compilation process I describe 
below will work on Windows machines.

Once properly compiled, the JAR will be runnable on any machine that has the JVM 
(Java Virtual Machine, commonly referred to as Java) installed.


This project is (theoretically) maintained by Benjamin Perlmutter at 
https://github.com/bperlmut/OutsReporter, which is a public repository.



How to work on this project:

1. Extract JAR contents

The files in the jar:

	- OutsReporter.java
		The actual code-base of the program.

	- jxl directory
		Used in 'makeJar' to create a jar, or java executable. 

		This is the contents of the external library Java Excel API. A new 
		version can be obtained by downloading jxl.jar (see below), then 
		expanding it. this jxl directory is the internal jxl directory from 
		jxl.jar version 2.6.12

	- Manifest.txt
		Required for 'makeJar' - Java requires a manifest when making a jar 
		file so that the JVM knows where the main function and classpath of 
		the program are

Included but not used:
	- jxl.jar (version 2.6.12)
		This may be used when running 'compile' so javac knows the location 
		of the external library (Java Excel API) used if compile is edited 
		to include the -classpath flag.
		
		I have edited the build in order to not require this, but have 
		included the entire thing in case future changes require it to be 
		present. At the time of this writing, it could be downloaded from 
		http://sourceforge.net/projects/jexcelapi/files/jexcelapi/


2. run 'compile' from inside the extracted directory
		Most .class files will be wiped out and replaced with those for the 
		newly updated code when you run the 'compile' command. In the case that 
		a build is malfunctioning, you may want to manually wipe out all .class 
		files to ensure that only the new ones remain.

3. run 'makeJar'
		This will re-package the jar, including all the files you found in it 
		to start with. 

4. run 'run'
		This will run OutsReporter, and is literally a call of
		'java -jar OutsReporter.jar'
		This is equivalent to double-clicking OutsReporter.jar
		




