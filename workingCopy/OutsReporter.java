/*
    Outs Reporter - A program written for Time Service Inc. intended to
	produce "Outs Reports" given a department inventory and an authorized
	inventory list.

	Copyright (C) 2016 Benjamin Perlmutter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.Thread;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import javax.swing.*;
import jxl.*;
import jxl.read.biff.BiffException;

public class OutsReporter {

 	public static JFrame mainWindow;
	public static Sheet sheet;
	public static int storeNum;

	public static Point findHeading(String heading) {
		int maxCol = sheet.getColumns();
		int maxRow = sheet.getRows();
		Cell c; 
		String contents;
		// look for where column headings start, beginning at 0,0
		// if heading is not found, return a non-existent cell point
		for (int col = 0; col < maxCol; col++) { // go across columns after down rows
			for (int row = 0; row < maxRow; row++) { // go down the row first
				c = sheet.getCell(col, row);

				// if the cell doesn't exist, stop looking in this row
				if (c == null) { break; }
				// if it does, look in it for the heading
				else { 
					contents = c.getContents();
					// stop when the heading is found
					if (Pattern.matches(heading, contents)) { 
						
						return new Point(col, row); }
				}
			}
		}

		// if the end of the loop is reached, the heading was not found
		return (new Point(-1,-1));
	}

	// actually generate the report
	public static void genReport(File inventoryFile, File authFile) {
		
		try {
			Workbook workbook = Workbook.getWorkbook(inventoryFile);
			sheet = workbook.getSheet(0);

			// find case-insensitive headings
			Point storeHeading = findHeading("(?i)store");
			Point skuHeading = findHeading("(?i)sku");
			Point quantityHeading = findHeading("(?i)quantity");
			Point statusHeading = findHeading("(?i)status");


			// if any of the headings weren't found, stop
			if (storeHeading.row == -1 || skuHeading.row == -1 || quantityHeading.row == -1 || statusHeading.row == -1) {
				JOptionPane.showMessageDialog(mainWindow, "Not all headings (store, sku, quantity, and status) were found.\n" + 
											  "Please format inventory file correctly.", "Inventory Formatting Error", 
											  JOptionPane.ERROR_MESSAGE);
				return;
			}
	
			// start inputting rows in the row immediately beneath the headings
			// use row relative to statusHeading because it's absolutely necessary
			int row = statusHeading.row + 1;
			Hashtable<String, String> inventory = new Hashtable<String, String>();
			while (row < sheet.getRows()) {
				
				String status = sheet.getCell(statusHeading.col, row).getContents();
				String sku = sheet.getCell(skuHeading.col, row).getContents();

				// could do something with quantity here to further confirm that the inventory file is formatted correctly

				// put all items into the table
				if (sku != "") {
					inventory.put(sku, status); 
					
					// check the store number is valid to confirm valid inventory file format
					int store = Integer.parseInt(sheet.getCell(storeHeading.col, row).getContents());
					if (store != storeNum) {
						JOptionPane.showMessageDialog(mainWindow, 
										"Inventory contains item from incorrect department or\n" +
										"inventory file name does not match department number\n" +
										"Row: " + Integer.toString(row + 1) + 
										"\n\nNo Outs Report was generated. Correct file to continue.",
										"Inventory Formatting/Content Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
				}
				row++;
			}

			// inventory now contains keys: SKU numbers; values: status

			String date = new SimpleDateFormat("yyyy MM dd").format(new Date());

			// generate a file-name for the outs report
			String outsFileName = inventoryFile.getParent() + File.separator 
								+ "Dept " + Integer.toString(storeNum) + " Outs Report " + date;
			String extension = ".csv";
			String outsFinalName = outsFileName + extension;

			// if the file-name already exists, pick a new one
			if (new File(outsFinalName).exists()) {
				int tryNum = 1;

				// try each successive number until a file name is found that isn't taken
				while ( new File(outsFileName + " rev " + Integer.toString(tryNum) + extension).exists() ) {
					tryNum++;
				}

				outsFinalName = outsFileName + " rev " + Integer.toString(tryNum) + extension;
			}

			// create the file
			PrintWriter outsFile = new PrintWriter(outsFinalName);
			outsFile.println("Department Number, Sears Store Number, Class, SKU, Sears SKU, Description, Our Price");


			// begin to parse the authFile
			java.util.List<String> lines = Files.readAllLines(authFile.toPath(), Charset.defaultCharset());
			int outCount = 0;


			// parse each line of the authFile to find all the items for *this* store
			for (int i = 0; i < lines.size(); i++) {
				
				String[] parts = lines.get(i).split(",");

				// remove all whitespace (and quotation marks where relevant) to account for inconsistent formatting
				int store = Integer.parseInt(parts[0].replaceAll("\\s+",""));
				String sku = parts[3].replaceAll("\\s+","").replace("\"", "");
				String searsSku = parts[4].replaceAll("\\s+","").replace("\"", "");

				// if the department for this line corresponds to the correct store, check on the item
				if (store == storeNum) { 
			
					// pad the SKU's so that they always have at least 6 digits
					// (this is a work-around because authFile's leading zeros were lost in excel)
					if (sku != "" && sku.length() < 6) {
						for (int pad = sku.length(); pad < 6; pad++) { sku = "0" + sku; }
					}
					// if the item is not listed in the inventory, then add it to the outs list
					if ( !inventory.containsKey(sku) && !inventory.containsKey(searsSku) ) {
						
						// currently outputting entire line from authFile that corresponds to the out-of-stock item
						outsFile.println(lines.get(i));
						outCount++;
					}
				}
			}
		
			// place a footer at the end of the list of items
			String dateTime = new SimpleDateFormat("yyyy MM dd - hh:mm a").format(new Date());
			outsFile.println("Items: " + Integer.toString(outCount));
			outsFile.println();
			outsFile.println(dateTime);

			outsFile.close();

			JOptionPane.showMessageDialog(mainWindow, 
						"Successfully created Outs Report for department " + Integer.toString(storeNum) + 
						"!\nIn file: " + outsFinalName, "Success!", JOptionPane.INFORMATION_MESSAGE);

		}
		catch (IOException|BiffException err) {
			JOptionPane.showMessageDialog(mainWindow, "unknown error in reading workbook");
			System.out.println("well, that's not great. not sure how to handle errors");	
		}
	}

	public static boolean correctFileName(String invFileName, String authFileName) {

		// expected inventory file name is one or more digits followed by .xls
		String correctInvName = "\\d+\\.xls";
		// expected auth file name is 'SKUSTROUT.csv'
		String correctAuthName = "SKUSTROUT\\.csv";

		// generate error messages and error pop-up title in case of errors
		// in file names
		String errorMessage = "";
		String errorTitle = "";
		int errorCount = 0;

		// check the inventory file
		if (!Pattern.matches(correctInvName, invFileName)) {
			errorMessage += "Bad Inventory File Name: \'" + invFileName + "\'" +
							"\nExpected something like \'123.xls' (contains only numbers)"+
							"\nName must be in form of [Department-Number].xls";
			errorTitle += "Inventory File-Name";
			errorCount++;

		} else { // if the inventory file-name matches the correct pattern, use it to get the store number
			try {
				storeNum = Integer.parseInt(invFileName.substring(0,invFileName.length() - 4));
			} catch (NumberFormatException ex) {
				// this should actually never happen as originally designed, because the inventory name's format
				// contains only numbers, and will be caught by the regular expression correctInvName, but
				// this may be necessary if the inventory file-name is to change in the future
				errorMessage += "Bad Inventory File Name: \'" + invFileName + "\'" +
							"\nExpected something like \'123.xls' (contains only numbers)"+
							"\nName must be in form of [Department-Number].xls";
				errorTitle += "Inventory File-Name";
				errorCount++;
			}
		}

		// check the authFile
		if (!Pattern.matches(correctAuthName, authFileName)){
			if (errorCount > 0) {
				errorMessage += "\n\nBad Authorized Inventory File Name: \'" + authFileName + "\'" +
								"\nExpected \'SKUSTROUT.csv\'";
				errorTitle += " and Authorized Inventory File-Name";
			} else {
				errorMessage += "Bad Authorized Inventory File Name: \'" + authFileName + "\'" +
								"\nExpected \'SKUSTROUT.csv\'";
				errorTitle += "Authorized Inventory File-Name";
			}
			errorCount++;
		}

		if (errorCount == 1) { errorTitle += " Error"; } else if (errorCount > 1) { errorTitle += " Errors"; }
	
		// if there were any errors, pop up a dialog box and return false (at least one incorrect file name)
		if (errorCount > 0) {
			
			errorMessage += "\n\nNo Outs Report was generated.\nSelect correctly-named files to continue.";

			JOptionPane.showMessageDialog(mainWindow, errorMessage, errorTitle, JOptionPane.ERROR_MESSAGE);
			return false;	
		} else { // if no errors were found, return true (file names are correct)
			return true;
		}
	}

	// check for file-name errors and generate report
	public static void generateOutsReport(String inventoryPath, String authPath) {

		File invFile = new File(inventoryPath);
		File authFile = new File(authPath);
		
		// check that both files were actually selected and stop if they weren't
		if (!invFile.isFile() || !authFile.isFile()) {
			JOptionPane.showMessageDialog(mainWindow, "Please select all files to proceed.\nNo Outs Report was generated.", 
						"File-Selection Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		String invFileName = invFile.getName();
		String authFileName = authFile.getName();

		// confirm that file names and formats are correct - if correct file names were not found, stop generating
		if (!correctFileName(invFileName, authFileName)) {
			return;
		}

		// generate the report
		genReport(invFile, authFile);
		// genReport pops up a finished dialog when outs report is successfully completed

	}

	// make a file chooser with the given title
	public static JFileChooser pickFile(String title) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(title);
      	chooser.setCurrentDirectory(new java.io.File("."));
       	chooser.setSelectedFile(new File(""));
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		return chooser;
	}

	// confirm whether the filename listed in a JTextField corresponds to a real file
	public static boolean namesValidFile(JTextField tf) {
		// if this is a valid filename, return true
		if (new File(tf.getText()).isFile()) { return true; } 
		
		// otherwise, return false
		else { return false; }
		
	}

	// color a text field based on whether the field contains a valid file or not
	public static void colorConfirmTextField(JTextField tf) {
		if (namesValidFile(tf)) { // if the field contains a valid file, color it green
			tf.setBorder(BorderFactory.createLineBorder(new Color(0,112,0), 2));
		} else { // otherwise, color it red
			tf.setBorder(BorderFactory.createLineBorder(new Color(209,0,21), 2));
		}
	}

	// make a text field with behavior and size based on passed parameters
	public static JTextField makeTextField(String contents, int width, final String chooserTitle) {
		
		final JTextField tf = new JTextField(contents, width);
	
		// make the text field do something when the mouse is clicked on it
		tf.addMouseListener(new MouseListener() {

			String text = "";
			boolean openDialog = false;
		
			// when the mouse is clicked on the text field, open a dialog box
			public void mouseClicked(MouseEvent e){

					// save the text as whatever text was in the box
					text = tf.getText();
				
					// Change the text in the box temporarily when clicked
					File f = new File (tf.getText());
					if (!f.isFile()) {
						tf.setText("Waiting for file selection...");
					} else {
						tf.setText("Selecting a new file... Press cancel to keep old file.");
					}

					// only open a dialog box if one doesn't already exist
					// (This is probably unnecessary, but is safer UX wise)
					if (!openDialog) {
						openDialog = true;

						// make a dialog box
						JFileChooser chooser = pickFile(chooserTitle);

						// if the box's APPROVE_OPTION is clicked, confirm the file name
						if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
							f = chooser.getSelectedFile();
							
							// get a string for the file name
							String fileName;
							try { fileName = f.getCanonicalPath(); }
							catch (IOException err) { fileName = f.getAbsolutePath(); }

							// double-check if the file is real and is a file.
							// If it is, the path displayed will be set to its path
							if (f.isFile()) { text = fileName; }

						} 

						openDialog = false;
					}	
		
					// set the box to disply the selected file name (or the old contents, if no file was selected)
					tf.setText(text);
			}

			// if the mouse is pressed (but not released) set the text
			// back so the value can't be lost with stray right clicks.
			public void mousePressed(MouseEvent e) { 
				tf.setText(text);
			}

			// Display reminder text in text box when user scrolls mouse over it
			public void mouseEntered(MouseEvent e) {
				text = tf.getText();
				// don't immediately display the reminder text to prevent the UI from seeming 'jumpy'
				try { Thread.sleep(150); } catch(InterruptedException ex) {}
				if (namesValidFile(tf)) {
					tf.setText("Click to select different file...");	
				} else {
					tf.setText("Click to select file...");
				}
			}

			// Restore text in box after mouse leaves
			public void mouseExited(MouseEvent e) {
				tf.setText(text);
			}

			// Java syntax requires that all five MouseListener methods be defined explictly
			public void mouseReleased(MouseEvent e) {}
		});
		
		return tf;
	}

	// create a window for the program
	public static JFrame makeWindow() {
		
		// new a window is a JFrame, with a fixed size, location, and close operation
		final JFrame win = new JFrame("Outs Reporter");
		win.setSize(620,125);
		win.setResizable(false);
		win.setLocationByPlatform(true);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// User Interface labels and strings
		String[] labels = {"Store Inventory File: ", "Authorized Inventory File: "};
		final String reminderText = "Click to select file...";
		String[] chooserTexts = {"Select a Store Inventory File...", "Select a Authorized File"};

		final Vector<JTextField> textFields = new Vector<JTextField>();

		// make a panel for all the buttons and everything
		JPanel panel = new JPanel();

		// for each label, the user must provide a file
		for (int i = 0; i < labels.length; i++) {
	
			// make the label and text field
			JLabel label = new JLabel(labels[i], JLabel.TRAILING);
			panel.add(label);
			JTextField textField = makeTextField(reminderText, 35, chooserTexts[i]);
			label.setLabelFor(textField);

			// add the label and text field to the panel and vector of textFields
			textFields.add(textField);
			panel.add(textField);

		}

		/*
		// make an instructions button
		JButton instructionsButton = new JButton("Instructions");
			instructionsButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					final JFrame instructionsWin = new JFrame();
					instructionsWin.setTitle("Instructions");
					//instructionsWin.setSize(300,200);
					//instructionsWin.setLocationRelativeTo(null);
			
					JLabel instructions = new JLabel("words and lots oand lots ofasldkfjalskdfj alsdk");
					instructions.setLabelFor(null);
					instructionsWin.add(instructions);

					JButton closeButton = new JButton();
					closeButton.setText("Close Instructions");	
					closeButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) { instructionsWin.dispose(); }
					});	
					instructionsWin.add(closeButton);

					instructionsWin.pack();
					instructionsWin.setVisible(true);
			}
		});
		panel.add(instructionsButton);
		*/

		// make a reset button
		JButton resetButton = new JButton("Reset Form");
		// reset button sets all fields back to their reminderText (initial) values 
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < textFields.size(); i++) {
					textFields.get(i).setText(reminderText);
				}
			}
		});
		panel.add(resetButton);


		JButton generateButton = new JButton("Generate Outs Report");
		generateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// get the file names listed
				String inventoryPath = textFields.get(0).getText();
				String authPath = textFields.get(1).getText();

				// attempt to generate the report
				generateOutsReport(inventoryPath, authPath);
			
			}
		});
		panel.add(generateButton);	
		
		// add the entire panel to the window
		win.add(panel);
		
		// make window visible
		win.setVisible(true);
		resetButton.requestFocus();

		// return the window so we can still access it
		return win;
	}

    public static void main(String[] args) {
		
		mainWindow = makeWindow();

	}

}
