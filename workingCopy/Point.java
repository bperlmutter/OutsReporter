/*
    Outs Reporter - A program written for Time Service Inc. intended to
	produce "Outs Reports" given a department inventory and an authorized
	inventory list.

	Copyright (C) 2016 Benjamin Perlmutter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Point {
	public Point(int c, int r) {
		row = r;
		col = c;
	}
	public Integer row;
	public Integer col = -2;
	public void print() {
		if (row == null || col == null) {
			System.out.println("point not initialized");
		} else {
			System.out.println("("+col+", "+row+")");
		}
	}
}
